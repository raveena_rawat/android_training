package com.example.user_profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public final static String User_Name="com.example.user_profile.user_name";
    public final static String Email_id="com.example.user_profile.email_id";
    public final static String Contact_no="com.example.user_profile.contact_no";
    public final static String Location="com.example.user_profile.location";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.btn_submit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity2();
            }
        });
    }
        public void openActivity2()
        {
            EditText et_user_name=(EditText)findViewById(R.id.et_user_name);
            String user_name=et_user_name.getText().toString();

            EditText et_email_id=(EditText)findViewById(R.id.et_email_id);
            String email_id=et_email_id.getText().toString();

            EditText et_location=(EditText)findViewById(R.id.et_location);
            String location=et_location.getText().toString();

            EditText et_contact_no=(EditText)findViewById(R.id.et_contact_no);
            Integer contact_no=Integer.parseInt(et_contact_no.getText().toString());

            Intent intent=new Intent(this,DataReceiveActivity.class);
            intent.putExtra(User_Name,user_name);
            intent.putExtra(Email_id,email_id);
            intent.putExtra(Location,location);
            intent.putExtra(Contact_no,contact_no);
            startActivity(intent);
        }

}