package com.example.user_profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class DataReceiveActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_receive);

        Intent intent=getIntent();
        String User_Name=intent.getStringExtra(MainActivity.User_Name);

        int Contact_no=intent.getIntExtra(MainActivity.Contact_no,0);

        String Email_id=intent.getStringExtra(MainActivity.Email_id);

        String Location=intent.getStringExtra(MainActivity.Location);

        TextView tv_user_name=(TextView)findViewById(R.id.tv_user_nmae);

        TextView tv_email_id=(TextView)findViewById(R.id.tv_email_id);

        TextView tv_contact_no=(TextView)findViewById(R.id.tv_contact_no);

        TextView tv_location=(TextView)findViewById(R.id.tv_location);

        tv_user_name.setText(User_Name);
        tv_email_id.setText(Email_id);
        tv_contact_no.setText("" + Contact_no);
        tv_location.setText(Location);
    }
}