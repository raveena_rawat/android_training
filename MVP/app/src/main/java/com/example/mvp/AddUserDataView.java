package com.example.mvp;

public interface AddUserDataView {
    void onSuccess();
    void onError_User_Name(String errorMessage);
    void onError_Email_Id(String errorMessage);

}
