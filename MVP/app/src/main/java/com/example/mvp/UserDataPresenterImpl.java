package com.example.mvp;

import android.text.TextUtils;
import android.util.Patterns;

import java.util.ArrayList;

public class UserDataPresenterImpl implements AddUserDataPresenter {
    private UserDataModelImpl model;
    private AddUserDataView view;
    String et_user_name="";
    String et_email_id="";
    ArrayList arrayList;
    public UserDataPresenterImpl(AddUserDataView view)
    {
        this.model=new UserDataModelImpl(et_user_name,et_email_id,arrayList);
        this.view=view;
    }
    public void update_User_Name(String et_user_name){
        model.setUser_Name(et_user_name);
    }
    public void update_Email_Id(String et_email_id){
        model.setEmail_Id(et_email_id);
    }
    @Override
    public void validate_User(String et_user_name, String et_email_id, ArrayList arrayList) {
        if (TextUtils.isEmpty(et_user_name))
            view.onError_User_Name("Please Enter UserName");
        else if (TextUtils.isEmpty(et_email_id))
            view.onError_Email_Id("Please Enter Email Id");
        else if(!Patterns.EMAIL_ADDRESS.matcher(et_email_id).matches())
        {
            view.onError_Email_Id("Please Enter A Valid Email Address");
        }
        else
        {
            arrayList.add("User Name:"+et_user_name+"  "+"Email Id:"+et_email_id);
        }
    }

}
