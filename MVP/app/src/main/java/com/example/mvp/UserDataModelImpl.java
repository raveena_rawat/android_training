package com.example.mvp;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import java.util.ArrayList;

public class UserDataModelImpl {
    String et_user_name = "";
    String et_email_id = "";
    ArrayList<String> arrayList;

    public UserDataModelImpl(String et_user_name, String et_email_id, ArrayList arrayList) {
        this.et_user_name = et_user_name;
        this.et_email_id = et_email_id;
        this.arrayList=arrayList;
    }
    public String getUser_Name() {
        return et_user_name;
    }

    public void setUser_Name(String et_user_name) {
        this.et_user_name = et_user_name;
    }

    public String getEmail_Id() {
        return et_email_id;
    }

    public void setEmail_Id(String et_email_id) {
        this.et_email_id = et_email_id;
    }
    public ArrayList<String> getArraylist() {
        return arrayList;
    }

    public void setArrayList(ArrayList arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public String toString() {
        return "Email : " + et_email_id + "\nFullName : " + et_user_name;
    }
}

