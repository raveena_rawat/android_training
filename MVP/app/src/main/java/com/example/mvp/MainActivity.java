package com.example.mvp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AddUserDataView{

    private UserDataPresenterImpl presenter;
    private Button btn_add_data;
    private CoordinatorLayout mCoordinatorLayout;
    EditText et_user_name,et_email_id;
    ListView lv_user_data;
    ArrayList<String> arrayList;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mCoordinatorLayout=(CoordinatorLayout)findViewById(R.id.coordinatorlayout);
        btn_add_data=(Button)findViewById(R.id.btn_add_data);
        presenter=new UserDataPresenterImpl(this);

        et_user_name=(EditText)findViewById(R.id.et_user_name);
        et_email_id=(EditText)findViewById(R.id.et_email_id);
        lv_user_data=(ListView)findViewById(R.id.lv_user_data);
        arrayList=new ArrayList<String>();
        adapter=new ArrayAdapter<>(MainActivity.this,android.R.layout.simple_list_item_1,arrayList);
        lv_user_data.setAdapter(adapter);

        et_user_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.update_User_Name(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et_email_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.update_Email_Id(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        btn_add_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(mCoordinatorLayout,"Adding Data in Progress",Snackbar.LENGTH_LONG).setAction("Please Confirm!", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presenter.validate_User(et_user_name.getText().toString(),et_email_id.getText().toString(),arrayList);
                        adapter.notifyDataSetChanged();
                    }
                }).show();
            }
        });
    }



    @Override
    public void onSuccess() {
        Toast.makeText(this,"Data Inserted",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onError_Email_Id(String errorMessage) {
        et_email_id.setError(errorMessage);
    }
    @Override
    public void onError_User_Name(String errorMessage) {
        et_user_name.setError(errorMessage);
    }

}