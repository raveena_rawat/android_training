package com.example.assignment_2v2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper myDb;
    EditText et_user_name,et_email_id,et_contact_no;
    Button btn_add_data,btn_view_all;
    TextView tv_user_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDb =new DatabaseHelper(this);
        et_user_name=(EditText) findViewById(R.id.et_user_name);
        et_email_id=(EditText)findViewById(R.id.et_email_id);
        et_contact_no=(EditText)findViewById(R.id.et_contact_no);
        btn_add_data=(Button)findViewById(R.id.btn_add_data);
        btn_view_all=(Button)findViewById(R.id.btn_View_all);
        tv_user_profile=(TextView)findViewById(R.id.tv_user_profile);
        tv_user_profile.setPaintFlags(tv_user_profile.getPaintFlags()|Paint.UNDERLINE_TEXT_FLAG);
        AddData();
        viewAll();
    }
    public void AddData(){
        btn_add_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               boolean isInserted= myDb.insertData(et_user_name.getText().toString(),
                        et_email_id.getText().toString(),
                        et_contact_no.getText().toString());
               if(isInserted=true)
               {
                   Toast.makeText(MainActivity.this,"Data Inserted",Toast.LENGTH_LONG).show();
               }
               else
               {
                   Toast.makeText(MainActivity.this,"Data Not Inserted",Toast.LENGTH_LONG).show();
               }
            }
        });
    }
    public void viewAll()
    {
        btn_view_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor cursor=myDb.getAllData();
                if(cursor.getCount()==0)
                {
                    showMessage("Error","Nothing Found");
                    return;
                }
                else
                {
                    StringBuffer buffer=new StringBuffer();
                    while (cursor.moveToNext()){
                        buffer.append("ID:"+cursor.getString(0)+"\n");
                        buffer.append("USER_NAME:"+cursor.getString(1)+"\n");
                        buffer.append("EMAIL_ID:"+cursor.getString(2)+"\n");
                        buffer.append("CONTACT_NO:"+cursor.getString(3)+"\n\n");
                    }
                    showMessage("Data",buffer.toString());
                }
            }
        });
    }
    public void showMessage(String title,String Message)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
}